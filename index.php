<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Marina Neri Machado</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body>
    <div id="container">
      <div id="top">
      <?php include "top.php";?>
      </div>
      <div id="menu">
      <?php include "menu.php"?>
      </div>
      <div id="content">
      <?php include "content.php"?>
      </div>
      <div id="footer">
      RODAPÉ
      </div>
    </div> <!-- container -->
  </body>
</html>

